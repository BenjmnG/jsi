---
title: Le sport associatif à Irigny
layout: index.html
tags: index
---
## Club Omnisports

Avec plus de 1300 adhérents, l'une des plus importante association de la commune, la Jeunesse Sportive d'Irigny compte 10 sections :
Aïkido, Aquagym et natation sportive adultes, Cyclotourisme, Football, Judo, Karaté, Kung-fu et défense pour tous, Natation et VTT.

Toutes les informations sont détaillées dans la page de chaque section.
