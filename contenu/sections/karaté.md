---
sport: karaté
nom: JSI Karaté

adresse: Dojo - 21 rue du stade<br>69540 Irigny

web: 
mail: alexia.tanlet@gmail.com
social: [["facebook", "jsikaratedo"]]

fondation: 1985 
federation: FFKAMA
president: Alexia Tanlet
tresorier: Marie-Laure Tanlet
secretaire: Quentin Tanlet

adherent: 80
couleur: rouge-sombre

---

La section karaté pratique le karaté style shotokan.
Ouvert à tous : petits et grands à partir de 6 ans - pour débutants, comme pour confirmés.

Horaires :
les mardis de 18h à 22h
les vendredis de 19h à 22h


N'hésitez pas à nous contacter pour plus de renseignements.
