---
sport: tennis
nom: JSI Tennis

adresse: Terrains de tennis - chemin de boutan<br>69540 Irigny

web: tcirigny.fr
mail: tcirigny@gmail.com
social: [["facebook", "Tennis.irigny"]]

fondation: 1979
federation: FFT
president: Antoine Plard
tresorier: Matthieu Saussine
secretaire: Maxime Butaud

adherent: 330
couleur: jaune
---

La section tennis accueille environ 300 adhérents chaque année sur les 5 courts municipaux (dont 2 couverts et 4 éclairés), à partir de 4 ans.

Au sein du club, vous pourrez jouer en pratique libre et en cours collectifs. 

L’équipe dirigeante et les enseignants proposent également des stages, du tennis à l'école avec les classes du primaire, des cours particuliers et des animations tout au long de l’année. 

Si vous êtes plus compétiteurs, vous pourrez participer à nos tournois interne et open (pour petits et grands). 
Des partenariats peuvent être faits avec les entreprises locales, pour une pratique dans le cadre de CE.

Vous trouverez toutes les informations utiles sur [le site internet](http://www.tcirigny.fr) ou sur Facebook.

Nous vous attendons pour une visite au club lors des permanences du samedi matin de 10h à 12h. »
