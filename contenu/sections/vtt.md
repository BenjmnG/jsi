---
sport: vtt
nom: JSI VTT

adresse: Fort de Montcorin <br>3 chemin des châtaigniers <br>69540 Irigny

web: irignyvtt.com
mail: 
social: [["facebook", "irignyvtt"]]

fondation: 1995
federation: FFC
president: Damien Vassault
tresorier: Camille Bouvard
secretaire: Julien Rage

adherent: 70
couleur: rouge
---

Vous souhaitez partager un bon moment sur votre VTT avec d'autres amateurs ?
Venez pratiquer au sein de notre club.

Vous pourrez participer à nos sorties depuis le niveau débutant jusqu'au pratiquants engagés dans les compétitions nationales.
Nous accueillons les jeunes au sein de notre école VTT (8/16 ans)
Nous accueillon aussi les adultes au sein de groupes de différents niveaux et différentes disciplines.
Nous possédons également un bike park réservé à nos adhérents pour des pratiques plus exclusives comme la descente, le "dirt", le "4X" ou le trial.
