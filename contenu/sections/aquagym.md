---
sport: aquagym
nom: JSI aquagym et natation sportive

adresse: Piscine - Chemin de Champvillard<br>69540 Irigny

web: jsi-asso.fr
mail: aquagymnatation.irigny@gmail.com
social: 

fondation: 2000
federation: 
president: Estelle Arnoux
tresorier: Anne-Marie Etienne
secretaire: Marie-Christine Vandenberghe

adherent: 280
couleur: bleu
---

À la piscine municipale d'Irigny notre section sportive, réservée aux adultes, privilégie la convivialité.

## Aquagym

Plus de 250 personnes participent aux dix séances hebdomadaires dans une ambiance détendue. 
Animés par 3 moniteurs, des créneaux horaires sont proposés en journée du lundi au samedi.

A noter qu'il n’est pas nécessaire de savoir nager pour pratiquer l’aquagym.

## Natation sportive adultes

En fonction de leur aptitude, une trentaine de nageurs se maintient en condition physique tous les lundis soirs. 
Un bon niveau de natation (4 nages) est requis.

## Inscriptions 

Le nombre de places est limité; il faut impérativement venir début septembre au forum des associations.   
<br>
Pour plus d’informations : [aquagymnatation.irigny@gmail.com](mailto:aquagymnatation.irigny@gmail.com)   
  
<br>
[Règlement intérieur](https://drive.google.com/file/d/1aqUEeiimO_4yDoZhwM_Zin5ofTk1EFp_/view)   
<br>
[Bienfaits de l'Aquagym](https://drive.google.com/file/d/1N_YqsEOQGf7KWS2hGNYo3vS-txISaN_w/view)
