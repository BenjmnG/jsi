---
sport: football
nom: JSI Foot

adresse: Stade - 2 rue de la mouche<br>69540 Irigny

web: jsifoot.com
mail: jsifootinscriptions@gmail.com
social: [["facebook", "JS-Irigny-Foot-Officiel-113930046662264"]]

fondation: 1960
federation: FFF
president: François Bouzezait
tresorier: Nisrine Zeroual
secretaire: Florent Abner

adherent: 240
couleur: vert
---

La section Foot est l'une des plus importante de la JSI et la plus actives associations de la commune d’Irigny. 

Riche de plus de cinquante ans d’histoire, la section compte environ 240 licenciés partagés entre plusieurs équipes allant de l’école de foot jusqu’aux vétérans. 

Sa devise immuable est l’épanouissement par la pratique du football dans le respect des coéquipiers et des adversaires. 

C’est un club familial ouvert à toutes les Irignoises et tous les Irignois, ce qui ne l’empêche pas de viser l’excellence au niveau des résultats. Son équipe fanion – les seniors 1 - évolue en première division du District du Rhône de football, et tous les espoirs sont permis pour une accession, dans un futur proche, au niveau supérieur.

Que vous soyez jeune ou adulte, vous trouverez à coup sûr une catégorie féminine ou masculine pour pratiquer pleinement et  passionnément votre sport favori.
