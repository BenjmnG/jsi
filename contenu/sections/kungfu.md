---
sport: kungfu
nom: JSI Kung fu & Défense pour tous

adresse: Dojo 21 rue du stade<br>Irigny

web: 
mail: boxechinoiseirigny@gmail.com
social: [["facebook", "boxechinoiseirigny"]]

fondation: 1985
federation: FFKDA
president: Valentin Comte
tresorier: Nicolas Gandy
secretaire: Christine Catarino-Roussilloux

adherent: 70
couleur: rouge
---


Le style de KUNG-FU pratiqué est un style du sud,
 
le HUNG GAR, ainsi que le SANDA (BOXE CHINOISE) partie combat du KUNG-FU.

Les cours sont dispensés par SIFU Hakim BOUAMRANE 6ème DUAN et Jean-Luc Milourat 2ème DUAN, un de ses élève diplômé

Pour la DEFENSE POUR TOUS (défense de rue) : obtenir des automatismes de défense est l’objectif des cours.

PAS DE PASSAGE DE GRADE… 

COURS LUDIQUES OUVERTS A TOUS DES 15 ANS dispensés par Jean-Luc Milourat, diplômé, formé en krav maga, formations et stages spécialisés. 

Pour tout renseignement : Jean Luc 06.42.36.02.98
