---
title: le mot du président
layout: info.html
permalink: le_mot_du_president.html
tags: info
couleur: vert-sombre
---


Je suis très heureux de présider cette très belle association, qui grâce au dévouement de ses dirigeants bénévoles et au professionnalisme de son encadrement permet aux jeunes et aux moins jeunes de pratiquer, à Irigny, leur sport favori.

Les sections disposent d'installations de grande qualité, à Champvillard, à Montcorin et à Yvours, mises à disposition par la municipalité.
Elles proposent : entrainements, compétitions locales régionales, voire nationale, stages ou encore en loisirs.

Je vous encourage à prendre directement contact avec les sections afin de choisir la discipline qui vous convient le mieux.
Bien sportivement
