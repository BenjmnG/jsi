let env = process.env.ELEVENTY_ENV;
const markdownIt = require('markdown-it'),
      fm = require('front-matter');

if (env === "dev") { global.pluginSass = require("eleventy-plugin-sass") }

// 11ty configuration
module.exports = config => {

  config.setUseGitIgnore(false);

  // md snippet
  const md = new markdownIt({
    html: true,
    breaks: true,
    typographer:  false, 
  });

  function sortByName(values) {
    return values.slice().sort((a, b) => a.data.sport.localeCompare(b.data.sport))
  }

  config.addFilter('sortByName', sortByName)

  config.addPairedNunjucksAsyncShortcode("markdown", async content => {
    content = fm(content)
    let html = md.render(content.body)
    return html;
  });

  config.addCollection("sections", function(collectionApi) {
    return collectionApi.getFilteredByGlob("sections/*.md")
  });  

  // pass through !
  config.addPassthroughCopy({'./design/assets/font/' : 'assets/font'});
  config.addPassthroughCopy({'./design/assets/favicon/' : '/'});
  config.addPassthroughCopy({'./design/assets/social/' : '/social'});
  config.addPassthroughCopy({'./contenu/sections/images' : '/sections/images'});
  config.addPassthroughCopy({'./contenu/le_mot_du_president/*.png' : '/'});

  if (env === "dev") {
    //scss plugin
    config.addPlugin(pluginSass, {
      remap: true,
      cleanCSS: false,
      autoprefixer: false,
      outputDir: 'public/assets/css'
    });
  } else{
    config.addPassthroughCopy({'./design/assets/css/' : 'assets/css'});
  }

  return {
    markdownTemplateEngine: 'njk',
    dataTemplateEngine: 'njk',
    htmlTemplateEngine: 'njk',
    dir: {
      input: 'contenu',
      data: '',
      includes: '../design/layouts',
      output: 'public'
    }
  };
};
